#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub struct Vertex {
    position: [f32; 3],
    tex_coords: [f32; 2],
}

const SQRT3: f32 = 1.7320508;

// A collection of the unique points
pub const VERTICES: &[Vertex] = &[
    Vertex {
        position: [0.5, 0.0, 0.0],
        tex_coords: [1.0, 0.5],
    },
    Vertex {
        position: [0.25, 0.25 * SQRT3, 0.0],
        tex_coords: [0.75, 0.5 - (0.25 * SQRT3)],
    },
    Vertex {
        position: [-0.25, 0.25 * SQRT3, 0.0],
        tex_coords: [0.25, 0.5 - (0.25 * SQRT3)],
    },
    Vertex {
        position: [-0.5, 0.0, 0.0],
        tex_coords: [0.0, 0.5],
    },
    Vertex {
        position: [-0.25, -0.25 * SQRT3, 0.0],
        tex_coords: [0.25, 0.5 + (0.25 * SQRT3)],
    },
    Vertex {
        position: [0.25, -0.25 * SQRT3, 0.0],
        tex_coords: [0.75, 0.5 + (0.25 * SQRT3)],
    },
];

pub const INDICES: &[u16] = &[0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5];

use std::mem;

impl Vertex {
    const ATTRIBS: [wgpu::VertexAttribute; 2] = [
        wgpu::VertexAttribute {
            offset: 0,
            shader_location: 0,
            format: wgpu::VertexFormat::Float32x3,
        },
        wgpu::VertexAttribute {
            offset: mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
            shader_location: 1,
            format: wgpu::VertexFormat::Float32x2,
        },
    ];
    pub fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        wgpu::VertexBufferLayout {
            array_stride: std::mem::size_of::<Vertex>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &Self::ATTRIBS,
        }
    }
}

/////////////////////
// UNIFORM BUFFERS //
/////////////////////

pub const Z_NEAR: f32 = 0.1;
pub const Z_FAR: f32 = 100.0;

use super::math::Camera;
use glam::Mat4;

#[repr(C)]
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub struct CameraUniform {
    view_proj: [[f32; 4]; 4],
}

impl CameraUniform {
    pub fn new() -> Self {
        Self {
            view_proj: Mat4::IDENTITY.to_cols_array_2d(),
        }
    }
    pub fn update_view_proj(&mut self, camera: &Camera, aspect: f32) {
        self.view_proj = camera
            .view_projection_matrix(Z_NEAR, Z_FAR, aspect)
            .to_cols_array_2d();
    }
}
