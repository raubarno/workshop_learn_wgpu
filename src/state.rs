use super::buffers::Vertex;
use super::math;
use wgpu::util::DeviceExt;
use wgpu::VertexBufferLayout;
use winit::event::*;
use winit::window::Window;

use super::buffers::CameraUniform;

pub struct State {
    surface: wgpu::Surface,
    device: wgpu::Device,
    queue: wgpu::Queue,
    config: wgpu::SurfaceConfiguration,
    pub(crate) size: winit::dpi::PhysicalSize<u32>,
    color: wgpu::Color,
    window: Window,
    render_pipelines: [wgpu::RenderPipeline; 2],
    active_pipeline_id: usize,

    vertex_buffer: wgpu::Buffer,

    index_buffer: wgpu::Buffer,
    num_indices: u32,

    diffuse_bind_group: wgpu::BindGroup,

    camera: math::Camera,
    camera_uniform: CameraUniform,
    camera_buffer: wgpu::Buffer,
    camera_bind_group: wgpu::BindGroup,
    shift_key_down: bool,
}

fn camera_default() -> math::Camera {
    use glam::Vec3;
    math::Camera {
        v: math::OrientedPlane::from_vectors(Vec3::X, Vec3::Y, glam::vec3(0.0, 0.0, 1.0)).unwrap(),
        f: 1.2,
        eye_radius: 0.1,
    }
}

impl State {
    // Creating some of the wgpu types requires async code
    pub async fn new(window: Window) -> Self {
        let size = window.inner_size();

        // The instance is a handle to our GPU
        // Its main purpose is to create Adapters and Surfaces.
        // Backends::all => Vulkan + Metal + DX12 + Browser WebGPU
        let instance = wgpu::Instance::new(wgpu::Backends::all());

        // # Safety
        //
        // The surface needs to live as long as the window that created it.
        // State owns the window so this should be safe.
        let surface = unsafe { instance.create_surface(&window) };

        // The adapter is a handle to our virtual card.
        //     power_preference has two variants: LowPower, and HighPerformance.
        //          LowPower will pick an adapter that favors battery life, such as an integrated GPU.
        //          HighPerformance will pick an adapter for more power-hungry yet more performant GPU's
        //          such as a dedicated graphics card.
        //          WGPU will favor LowPower if there is no adapter for the HighPerformance option.
        //      The compatible_surface field tells wgpu to find an adapter that can
        //          present to the supplied surface.
        //      The force_fallback_adapter forces wgpu to pick an adapter that will work on all hardware.
        //      This usually means that the rendering backend will use a "software" system,
        //      instead of hardware such as a GPU.
        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::default(),
                compatible_surface: Some(&surface),
                force_fallback_adapter: false,
            })
            .await
            .unwrap();

        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    features: wgpu::Features::empty(),
                    limits: if cfg!(target_arch = "wasm32") {
                        wgpu::Limits::downlevel_webgl2_defaults()
                    } else {
                        wgpu::Limits::default()
                    },
                    label: None,
                },
                None,
            )
            .await
            .unwrap();

        let config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: surface.get_supported_formats(&adapter)[0],
            width: size.width,
            height: size.height,
            present_mode: wgpu::PresentMode::Fifo,
            alpha_mode: wgpu::CompositeAlphaMode::Auto,
        };

        surface.configure(&device, &config);

        let diffuse_bytes = include_bytes!("../data/dmitrios-dice.jpg");
        let (diffuse_bind_group, texture_bind_layout) =
            super::bind_group::texture_from_image_memory(&device, &queue, diffuse_bytes);

        let shader = device.create_shader_module(wgpu::include_wgsl!("shader.wgsl"));

        // -- Camera -- {

        let camera = camera_default();

        let mut camera_uniform = CameraUniform::new();
        camera_uniform.update_view_proj(&camera, size.width as f32 / size.height as f32);

        let camera_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Camera Buffer"),
            contents: bytemuck::cast_slice(&[camera_uniform]),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        });

        let camera_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                label: Some("camera_bind_group_layout"),
                entries: &[wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::VERTEX,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                }],
            });

        let camera_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("camera_bind_group"),
            layout: &camera_bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: camera_buffer.as_entire_binding(),
            }],
        });

        // -- }

        fn create_render_pipeline(
            device: &wgpu::Device,
            shader: &wgpu::ShaderModule,
            config: &wgpu::SurfaceConfiguration,
            suffix: &str,
            buffers: &[VertexBufferLayout],
            bind_group_layouts: &[&wgpu::BindGroupLayout],
        ) -> wgpu::RenderPipeline {
            let render_pipeline_layout =
                device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: Some("Render Pipeline Layout"),
                    bind_group_layouts,
                    push_constant_ranges: &[],
                });
            device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
                label: Some("Render pipeline"),
                layout: Some(&render_pipeline_layout),
                vertex: wgpu::VertexState {
                    module: &shader,
                    entry_point: &format!("vs_main_{}", suffix),
                    buffers,
                },
                fragment: Some(wgpu::FragmentState {
                    module: &shader,
                    entry_point: &format!("fs_main_{}", suffix),
                    targets: &[Some(wgpu::ColorTargetState {
                        format: config.format,
                        blend: Some(wgpu::BlendState::REPLACE),
                        write_mask: wgpu::ColorWrites::ALL,
                    })],
                }),
                // this controls how to interpret our vertices when converting them into triangles
                primitive: wgpu::PrimitiveState {
                    // every three vertices will correspond to one triangle
                    topology: wgpu::PrimitiveTopology::TriangleList,
                    strip_index_format: None,
                    // the triangle is facing forward if the vertices
                    // are arranged in a counter-clockwise direction
                    front_face: wgpu::FrontFace::Ccw,
                    // Triangles that are not considered facing forward are culled.
                    cull_mode: Some(wgpu::Face::Back),
                    // Setting this to anything other than Fill requires Features::NON_FILL_POLYGON_MODE
                    polygon_mode: wgpu::PolygonMode::Fill,
                    // Requires Features::DEPTH_CLIP_CONTROL
                    unclipped_depth: false,
                    // Requires Features::CONSERVATIVE_RASTERIZATION
                    conservative: false,
                },
                depth_stencil: None,
                multisample: wgpu::MultisampleState {
                    count: 1,
                    mask: !0,
                    alpha_to_coverage_enabled: false,
                },
                // How many array layers the render attachments can have.
                // We won't be rendering to array textures.
                multiview: None,
            })
        }
        let layouts = &[&texture_bind_layout, &camera_bind_group_layout];
        let render_pipeline_1 =
            create_render_pipeline(&device, &shader, &config, "1", &[Vertex::desc()], layouts);
        let render_pipeline_2 =
            create_render_pipeline(&device, &shader, &config, "2", &[], layouts);
        let render_pipelines = [render_pipeline_1, render_pipeline_2];

        let vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Vertex Buffer"),
            contents: bytemuck::cast_slice(super::buffers::VERTICES),
            usage: wgpu::BufferUsages::VERTEX,
        });

        let index_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Index Buffer"),
            contents: bytemuck::cast_slice(super::buffers::INDICES),
            usage: wgpu::BufferUsages::INDEX,
        });
        let num_indices = super::buffers::INDICES.len() as u32;

        Self {
            size,
            device,
            queue,
            config,
            surface,
            window,
            render_pipelines,
            active_pipeline_id: 0,
            color: wgpu::Color {
                r: 0.0,
                g: 0.5,
                b: 0.0,
                a: 1.0,
            },
            vertex_buffer,
            index_buffer,
            num_indices,
            diffuse_bind_group,
            camera,
            camera_uniform,
            camera_buffer,
            camera_bind_group,
            shift_key_down: false,
        }
    }

    pub fn window(&self) -> &Window {
        &self.window
    }

    pub fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        if new_size.width > 0 && new_size.height > 0 {
            self.size = new_size;
            self.config.width = new_size.width;
            self.config.height = new_size.height;
            self.surface.configure(&self.device, &self.config);
        }
    }

    pub fn camera_input(&mut self, event: &WindowEvent) -> bool {
        use glam::Mat3;
        use glam::Vec3;
        const UPDATE_ANGLE: f32 = 0.1;
        const UPDATE_MOVE: f32 = 0.1;
        let r = match event {
            WindowEvent::ModifiersChanged(modst) => {
                self.shift_key_down = modst.shift();
                false
            }
            WindowEvent::KeyboardInput {
                input:
                    KeyboardInput {
                        state,
                        virtual_keycode: Some(VirtualKeyCode::LShift) | Some(VirtualKeyCode::RShift),
                        ..
                    },
                ..
            } => {
                self.shift_key_down = state == &ElementState::Pressed;
                false
            }
            WindowEvent::KeyboardInput {
                input:
                    KeyboardInput {
                        state: ElementState::Pressed,
                        virtual_keycode: Some(keycode),
                        ..
                    },
                ..
            } => match (keycode, self.shift_key_down) {
                (VirtualKeyCode::Q, true) => {
                    let rmat = Mat3::from_axis_angle(glam::Vec3::Z, UPDATE_ANGLE);
                    self.camera.v = self.camera.v.rotated_xyz(rmat);
                    true
                }
                (VirtualKeyCode::E, true) => {
                    let rmat = Mat3::from_axis_angle(glam::Vec3::Z, -UPDATE_ANGLE);
                    self.camera.v = self.camera.v.rotated_xyz(rmat);
                    true
                }
                (VirtualKeyCode::W, true) => {
                    let rmat = Mat3::from_axis_angle(glam::Vec3::X, UPDATE_ANGLE);
                    self.camera.v = self.camera.v.rotated_xyz(rmat);
                    true
                }
                (VirtualKeyCode::S, true) => {
                    let rmat = Mat3::from_axis_angle(glam::Vec3::X, -UPDATE_ANGLE);
                    self.camera.v = self.camera.v.rotated_xyz(rmat);
                    true
                }
                (VirtualKeyCode::A, true) => {
                    let rmat = Mat3::from_axis_angle(glam::Vec3::Y, UPDATE_ANGLE);
                    self.camera.v = self.camera.v.rotated_xyz(rmat);
                    true
                }
                (VirtualKeyCode::D, true) => {
                    let rmat = Mat3::from_axis_angle(glam::Vec3::Y, -UPDATE_ANGLE);
                    self.camera.v = self.camera.v.rotated_xyz(rmat);
                    true
                }

                (VirtualKeyCode::Q, false) => {
                    self.camera.v = self.camera.v.moved(Vec3::Z * UPDATE_MOVE);
                    true
                }
                (VirtualKeyCode::E, false) => {
                    self.camera.v = self.camera.v.moved(Vec3::Z * -UPDATE_MOVE);
                    true
                }
                (VirtualKeyCode::W, false) => {
                    self.camera.v = self.camera.v.moved(Vec3::Y * UPDATE_MOVE);
                    true
                }
                (VirtualKeyCode::S, false) => {
                    self.camera.v = self.camera.v.moved(Vec3::Y * -UPDATE_MOVE);
                    true
                }
                (VirtualKeyCode::D, false) => {
                    self.camera.v = self.camera.v.moved(Vec3::X * UPDATE_MOVE);
                    true
                }
                (VirtualKeyCode::A, false) => {
                    self.camera.v = self.camera.v.moved(Vec3::X * -UPDATE_MOVE);
                    true
                }
                (VirtualKeyCode::U, true) => {
                    self.camera = camera_default();
                    true
                }
                _ => false,
            },
            _ => false,
        };
        if r {
            self.update()
        }
        r
    }

    pub fn input(&mut self, event: &WindowEvent) -> bool {
        match event {
            WindowEvent::CursorMoved { position, .. } => {
                self.color = wgpu::Color {
                    r: (position.x / self.size.width as f64).clamp(0., 1.0),
                    g: 0.5,
                    b: (position.y / self.size.height as f64).clamp(0., 1.0),
                    a: 1.0,
                };
                self.window.request_redraw();
                true
            }
            WindowEvent::KeyboardInput {
                input:
                    KeyboardInput {
                        state: ElementState::Pressed,
                        virtual_keycode: Some(VirtualKeyCode::Space),
                        ..
                    },
                ..
            } => {
                self.active_pipeline_id = 1 - self.active_pipeline_id;
                true
            }
            _ => self.camera_input(event),
        }
    }

    pub fn update(&mut self) {
        self.camera_uniform.update_view_proj(
            &self.camera,
            self.size.width as f32 / self.size.height as f32,
        );
        self.queue.write_buffer(
            &self.camera_buffer,
            0,
            bytemuck::cast_slice(&[self.camera_uniform]),
        )
    }

    pub fn render(&mut self) -> Result<(), wgpu::SurfaceError> {
        // The get_current_texture function will wait for the surface
        //   to provide a new SurfaceTexture that we will render to.
        //   We'll store this in output for later.
        let output = self.surface.get_current_texture()?;

        // This line creates a TextureView with default settings.
        // We need to do this because we want to control
        //   how the render code interacts with the texture.
        let view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        // We also need to create a CommandEncoder to create
        //   the actual commands to send to the gpu.
        // Most modern graphics frameworks expect commands to be stored
        //   in a command buffer before being sent to the gpu.
        // The encoder builds a command buffer that we can then send to the gpu.
        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render encoder"),
            });

        // Now we can get to clearing the screen (long time coming).
        // We need to use the encoder to create a RenderPass.
        // The RenderPass has all the methods for the actual drawing.
        // The code for creating a RenderPass is a bit nested, so I'll copy
        // it all here before talking about its pieces.
        {
            // Result of `begin_render_pass()` borrows `encoder` mutably;
            // encoder.finish() requires an immutable borrow.
            //
            // The resolve_target is the texture that will receive the resolved output.
            // This will be the same as view unless multisampling is enabled.
            // We don't need to specify this, so we leave it as None.
            //
            // The ops field takes a wpgu::Operations object.
            // This tells wgpu what to do with the colors on the screen (specified by view).
            // The load field tells wgpu how to handle colors stored from the previous frame.
            // Currently, we are clearing the screen with a bluish color.
            // The store field tells wgpu whether we want to store
            //   the rendered results to the Texture behind our TextureView
            //   (in this case it's the SurfaceTexture).
            // We use true as we DO want to store our render results.
            // (store == whole window is treated as invalidated)
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("Render Pass"),
                color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                    view: &view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(self.color),
                        store: true,
                    },
                })],
                depth_stencil_attachment: None,
            });

            render_pass.set_pipeline(&self.render_pipelines[self.active_pipeline_id]);
            render_pass.set_bind_group(0, &self.diffuse_bind_group, &[]);
            render_pass.set_bind_group(1, &self.camera_bind_group, &[]);
            render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));

            if self.active_pipeline_id == 0 {
                render_pass
                    .set_index_buffer(self.index_buffer.slice(..), wgpu::IndexFormat::Uint16);
                render_pass.draw_indexed(0..self.num_indices, 0, 0..1);
            } else {
                render_pass.draw(0..12, 0..1);
            }
        }

        // submit will accept anything that implements IntoIter
        self.queue.submit(Some(encoder.finish()));
        output.present();

        Ok(())
    }
}
