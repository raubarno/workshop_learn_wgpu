/*
 * Copyright (c) 2023, Arnoldas Rauba. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     (1) Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     (2) Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *     (3)The name of the author may not be used to
 *     endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#![allow(dead_code)]

use glam::*;
use std::f32::consts;

pub type Real = f32;

#[derive(Clone, Copy, Debug)]
pub struct Ray {
    /// The starting point
    pub a: Vec3,
    /// The orientation plane
    pub p: Vec3,
    // x = a + t*p forall t in R
}

impl Ray {
    pub fn point_at(&self, t: Real) -> Vec3 {
        self.a + (self.p * t)
    }
}

#[derive(Clone, Copy, Debug)]
/// a*x + b*y + c*z - d*1 = 0
pub struct Plane(pub Vec4);

pub fn intersection(r: &Ray, p: &Plane) -> Option<Vec3> {
    // (a1*x + b1*y + c1*z + d1*1 = 0)  (p dot x = 0)
    // (x=a2+t*a3)
    // (y=b2+t*b3)
    // (z=b2+t*b3)
    // a1*(a2+t*a3) + b1*(b2+t*b3) + c1*(c2+t*c3) = d
    // t(a1*a3 + b1*b3 + c1*c3) = -d1*1 - a1*a2 - b1*b2 - c1*c2
    // t = -(a1*a2 + b1*b2 + c1*c2 + d1*1) / (a1*a3 + b1*b3 + c1*c3)
    // t = -(p dot r.a:[1.]) ./ (p dot r.p:[0.])
    let t = -p.0.dot(r.a.extend(1.)) / p.0.dot(r.p.extend(0.));
    if t.is_finite() && t.is_sign_positive() {
        Some((r.p * t) + r.a)
    } else {
        None
    }
}

/// OrientedPlane is a plane that contains unit vectors
#[derive(Clone, Copy, Debug)]
pub struct OrientedPlane {
    pub p: Plane,
    pub unit: Mat3,
    pub off: Vec3,
    pub poff: Vec3,
}

impl OrientedPlane {
    pub fn right_jhat(ihat: Vec3, jhat: Vec3) -> Option<Vec3> {
        // assume that ihat and jhat are normalised
        let prod = ihat.dot(jhat);
        let icat = prod * ihat;
        let jcat = jhat - icat;
        jcat.try_normalize()
    }
    pub fn from_vectors(ihat: Vec3, jhat: Vec3, off: Vec3) -> Option<Self> {
        let ihat = ihat.try_normalize()?;
        let jhat = jhat.try_normalize()?;
        let jhat = Self::right_jhat(ihat, jhat)?;
        let khat = ihat.cross(jhat);
        // off dot khat = d
        let d = off.dot(khat);
        let p = Plane(khat.extend(-d));
        let unit = Mat3::from_cols(ihat, jhat, khat);
        let poff = Self::static_position(unit, off);
        Some(OrientedPlane { p, unit, off, poff })
    }
    pub fn rotated(&self, angle_rad: Real) -> Self {
        let ihat_0 = self.unit.x_axis;
        let jhat_0 = self.unit.y_axis;
        let ihat = angle_rad.cos() * ihat_0 + angle_rad.sin() * jhat_0;
        let jhat = angle_rad.cos() * jhat_0 + angle_rad.sin() * -ihat_0;
        let khat = self.unit.z_axis;
        let unit = Mat3::from_cols(ihat, jhat, khat);
        let poff = Self::static_position(unit, self.off);

        Self {
            p: self.p,
            unit,
            off: self.off,
            poff,
        }
    }
    pub fn rotated_xyz(&self, rotation_matrix: Mat3) -> Self {
        let unit = self.unit * rotation_matrix;
        let poff = Self::static_position(unit, self.off);
        Self {
            p: self.p,
            unit,
            off: self.off,
            poff,
        }
    }
    pub fn moved(&self, off: Vec3) -> Self {
        let unit = self.unit;
        let p = self.p;
        let off = self.off + unit.mul_vec3(off);
        let poff = Self::static_position(unit, off);
        Self { p, unit, off, poff }
    }
    pub fn planar_position(&self, a: Vec3) -> Vec3 {
        Self::static_position(self.unit, a) - self.poff
    }
    pub fn static_position(unit: Mat3, a: Vec3) -> Vec3 {
        unit.mul_vec3(a)
        //Vec3::new(unit.x_axis.dot(a), unit.y_axis.dot(a), unit.z_axis.dot(a))
        // [y] dot i = a
    }
}

#[derive(Clone, Debug)]
pub struct Camera {
    pub v: OrientedPlane,
    pub f: Real,
    pub eye_radius: Real,
}

use glam::Mat4;
impl Camera {
    pub fn fov_deg(&self) -> Real {
        self.angular_size_rad(consts::SQRT_2).to_degrees()
    }
    pub fn angular_size_rad(&self, r: Real) -> Real {
        (r / self.f).atan()
    }
    pub fn focal_position(&self, pt: Vec3) -> Option<Vec2> {
        let a = self.v.planar_position(pt);
        (a.z == 0.).then(|| (a.truncate() / a.z) * self.f)
    }
    pub fn view_projection_matrix(&self, z_near: Real, z_far: Real, aspect: Real) -> Mat4 {
        let view = Mat4::look_to_rh(self.v.off, -self.v.unit.z_axis, self.v.unit.y_axis);
        let perspective = Mat4::perspective_rh(self.angular_size_rad(1.0), aspect, z_near, z_far);
        perspective * view
    }
}
