///////////////////
// Vertex shader //
///////////////////

struct CameraUniform {
    view_proj: mat4x4<f32>,
}

@group(1) @binding(0)
var<uniform> camera: CameraUniform;

struct VertexInputTex {
    @location(0) position: vec3<f32>,
    @location(1) tex_coords: vec2<f32>
}

struct VertexOutputTex {
    @builtin(position) clip_pos: vec4<f32>,
    @location(0) tex_coords: vec2<f32>,
};

struct VertexOutputCol {
    @builtin(position) clip_pos: vec4<f32>,
    @location(0) vertex_pos: vec3<f32>,
    @location(1) colour: vec3<f32>,
};

@vertex
fn vs_main_1(
    model: VertexInputTex,
) -> VertexOutputTex {
    var out: VertexOutputTex;
    out.tex_coords = model.tex_coords;
    out.clip_pos = camera.view_proj * vec4<f32>(model.position, 1.0);
    return out;
}

@vertex
fn vs_main_2(
    @builtin(vertex_index) in_vertex_index: u32,
) -> VertexOutputCol {
    var out: VertexOutputCol;
    let sidx = i32(in_vertex_index);
    let vidx = f32(sidx % 3);
    let tidx = f32(sidx / 3);
    let PHI : f32 = 3.14159265358979323 * 0.666666666666;
    let THETA : f32 = 0.1;
    let phase = (vidx * PHI) + (tidx * THETA);
    let x = cos(phase) * 0.5;
    let y = sin(phase) * 0.5;
    out.clip_pos = vec4<f32>(x, y, 0.0, 1.0);
    out.vertex_pos = out.clip_pos.xyz;
    return out;
}

/////////////////////
// Fragment shader //
/////////////////////

// These things are the 'uniforms'.
// We'll get into them later.
@group(0) @binding(0)
var t_diffuse: texture_2d<f32>;
@group(0) @binding(1)
var s_diffuse: sampler;

@fragment
fn fs_main_1(in: VertexOutputTex) -> @location(0) vec4<f32> {
    return textureSample(t_diffuse, s_diffuse, in.tex_coords);
}

@fragment
fn fs_main_2(in: VertexOutputCol) -> @location(0) vec4<f32> {
    return vec4<f32>((in.vertex_pos[0] + 1.0) * 0.5, 0.5, 0.5, 1.0);
}
